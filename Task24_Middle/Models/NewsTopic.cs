﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class NewsTopic : Entity
    {
        private static int Count { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        static NewsTopic()
        {
            Count = 0;
        }
        public NewsTopic ()
        {
            Count++;
            Id = Count;
        }
    }
}