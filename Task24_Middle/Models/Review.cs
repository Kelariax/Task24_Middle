﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Review : Entity
    {
        private static int Count { get; set; }
        [Required(ErrorMessage = "Field shouldnt be empty")]
        public string Author { get; set; }
        [StringLength(300, MinimumLength = 5, ErrorMessage = "Text length should be in range from 5 to 300 symbols")]
        [Required(ErrorMessage = "Field shouldnt be empty")]
        public string Text { get; set; }
        public DateTime Date { get; set; }
        static Review()
        {
            Count = 0;
        }
        public Review()
        {
            Count++;
            Id = Count;
            Date = DateTime.Now;
        }
    }
}