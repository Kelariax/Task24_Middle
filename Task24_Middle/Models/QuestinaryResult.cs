﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class QuestinaryResult : Entity
    {
        private static int Count { get; set; }
        [Display(Name = "Name: ")]
        [Required(ErrorMessage = "Field shouldnt be empty")]
        public string Name { get; set; }

        [Display(Name = "Surname: ")]
        [Required(ErrorMessage = "Field shouldnt be empty")]
        public string Surname { get; set; }

        [Display(Name = "Programming language: ")]
        [Required(ErrorMessage = "At least one checkbox should be selected")]
        public string ProgrammingLanguage { get; set; }

        [Display(Name = "Have expirience: ")]
        [Required]
        public bool HaveExpirience { get; set; }

        [Display(Name = "Studying: ")]
        [Required]
        public bool Studying { get; set; }
        static QuestinaryResult()
        {
            Count = 0;
        }
        public QuestinaryResult()
        {
            Count++;
            Id = Count;
        }
    }
}