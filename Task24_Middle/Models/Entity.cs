﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task24_Middle.Models
{
    public abstract class Entity
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
    }
}