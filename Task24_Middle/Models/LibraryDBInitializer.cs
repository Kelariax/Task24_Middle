﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class LibraryDBInitializer : DropCreateDatabaseAlways<LibraryContext>
    {
        protected override void Seed(LibraryContext db)
        {

            db.News.Add(new NewsTopic { Title = "What is Lorem Ipsum?", Text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", Date = new DateTime(2020, 5, 8) });
            db.News.Add(new NewsTopic { Title = "Why do we use it?", Text = "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.", Date = new DateTime(2021, 1, 12) });
            db.News.Add(new NewsTopic { Title = "Where does it come from?", Text = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.", Date = new DateTime(2021, 5, 28) });

            db.Reviews.Add(new Review { Author = "Johnny", Text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date = new DateTime(2020, 5, 8) });
            db.Reviews.Add(new Review { Author = "Derek", Text = "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.", Date = new DateTime(2021, 1, 12) });
            db.Reviews.Add(new Review { Author = "Danny", Text = "Contrary to popular belief, Lorem Ipsum is not simply random text.", Date = new DateTime(2021, 5, 28) });
        
            base.Seed(db);
        }
    }
}