﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() 
            :base("LibraryDB")
        { }
        public DbSet<NewsTopic> News { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<QuestinaryResult> QuestinaryResults { get; set; }

    }
}