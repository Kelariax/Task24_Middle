﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class HomeController : Controller
    {
        LibraryContext db = new LibraryContext();
        public ActionResult Index()
        {
            IEnumerable<NewsTopic> news = db.News;
            ViewBag.News = news;
            return View();
        }
    }
}