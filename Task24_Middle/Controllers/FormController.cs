﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class FormController : Controller
    {
        LibraryContext db = new LibraryContext();
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Form(QuestinaryResult result)
        {
            if (ModelState.IsValid)
            {
                TempData["result"] = result;
                db.QuestinaryResults.Add(result);
                db.SaveChanges();
                return RedirectToAction("FormResult");
            }
            return View();
        }
        public ActionResult FormResult()
        {
            return View(TempData["result"] as QuestinaryResult);
        }
    }
}