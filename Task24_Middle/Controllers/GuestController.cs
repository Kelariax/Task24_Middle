﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class GuestController : Controller
    {
        LibraryContext db = new LibraryContext();
        [HttpGet]
        public ActionResult Guest()
        {
            IEnumerable<Review> reviews = db.Reviews;
            ViewBag.Reviews = reviews;
            return View();
        }
        [HttpPost]
        public ActionResult Guest(Review review)
        {
            if (ModelState.IsValid)
            {
                db.Reviews.Add(review);
                db.SaveChanges();
            }
            IEnumerable<Review> reviews = db.Reviews;
            ViewBag.Reviews = reviews;
            return View();
        }
    }
}